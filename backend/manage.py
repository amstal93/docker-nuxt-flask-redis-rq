import redis
from flask_script import Server, Manager
from rq import Worker, Queue, Connection
from uwsgi import app

listen = ['high', 'default', 'low']

manager = Manager(app)
manager.add_command(
    'runserver',
    # Server(port=5000, use_debugger=True, use_reloader=True))
    Server(port=9061, use_debugger=True, use_reloader=True))
    # flask is on 9061 via docker

@manager.command
def runworker():
    redis_url = app.config['REDIS_URL']
    redis_connection = redis.from_url(redis_url)
    with Connection(redis_connection):
        #worker = Worker(app.config['QUEUES'])
        worker = Worker(map(Queue, listen))
        worker.work()


if __name__ == '__main__':
    manager.run()
